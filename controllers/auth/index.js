const { isEmail } = require('validator');
const Admin = require('../../models/admin');

const form = (req, res) => {
    return res.render('login-form', { layout: 'login' });
}

const login = async (req, res) => {
    try {
        const { email, password } = { ...req.body };

        console.log(req.body);
        // if (!username || !password || !isEmail(username)) {
        if (!email || !password) {
            return res.render('login-form', { layout: 'login', error: true });
        }

        const user = await Admin.login(email, password);

        console.log('user');
        console.log(user);

        if (!user) {
            return res.render('login-form', { layout: 'login', error: true });
        }

        req.session.user = {
            email: user.email,
            authenticated: true
        };

        return res.redirect(`/`);
    } catch (err) {
        console.log(err);
        return res.render('login-form', { layout: 'login', error: true });
    }
}

const logout = (req, res) => {
    req.session.destroy();
    return res.redirect('/auth/login');
}

module.exports = { form, login, logout };