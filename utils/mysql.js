const Promise = require('bluebird');
const mysql = require('mysql');

let db = null;

const mysqlQuery = function (...params) {
    return new Promise(function (resolve, reject) {
        if (!db) {
            db = mysql.createConnection({
                host: '127.0.0.1',
                user: 'root',
                password: '',
                database: 'player'
            });
        }
        const mQuery = db.format(...params);

        db.query(mQuery, function (err, res) {
            return err ? reject(err) : resolve(res);
        });
    });
};

module.exports = mysqlQuery;