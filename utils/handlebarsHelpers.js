const Handlebars = require('handlebars');
const moment = require('moment-timezone');
module.exports = {
    ifCond: function (v1, operator, v2, options) {
        switch (operator) {
            case '==':
                return (v1 == v2) ? options.fn(this) : options.inverse(this);
            case '===':
                return (v1 === v2) ? options.fn(this) : options.inverse(this);
            case '!=':
                return (v1 != v2) ? options.fn(this) : options.inverse(this);
            case '!==':
                return (v1 !== v2) ? options.fn(this) : options.inverse(this);
            case '<':
                return (v1 < v2) ? options.fn(this) : options.inverse(this);
            case '<=':
                return (v1 <= v2) ? options.fn(this) : options.inverse(this);
            case '>':
                return (v1 > v2) ? options.fn(this) : options.inverse(this);
            case '>=':
                return (v1 >= v2) ? options.fn(this) : options.inverse(this);
            case '&&':
                return (v1 && v2) ? options.fn(this) : options.inverse(this);
            case '||':
                return (v1 || v2) ? options.fn(this) : options.inverse(this);
            default:
                return options.inverse(this);
        }
    },
    formatUTC: function (date) {
        // const utc = new Date(date + ' UTC').toISOString();
        // return moment.utc(utc).format('YYYY/MM/DD HH:mm:ss');
        // return moment.utc(date).format('YYYY/MM/DD HH:mm:ss');
        return moment.utc(date).format('DD/MM/YYYY HH:mm:ss');
    },
    formatDateToTZ: function (date, tz) {
        // return moment.utc(date).tz(tz).format('YYYY/MM/DD HH:mm:ss');
        return moment.utc(date).tz(tz).format('DD/MM/YYYY HH:mm:ss');
    },
    isChecked: function (val, permissions, term) {
        for (const perm of permissions) {
            if (Number(val) === perm[term]) {
                return "checked";
            }
        }
    },
    json: function (obj) {
        return new Handlebars.SafeString(JSON.stringify(obj));
    }
}