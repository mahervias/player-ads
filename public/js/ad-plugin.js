/*
 * Example ad plugin using the videojs-ads plugin.
 *
 * For each content video, this plugin plays one preroll and one midroll.
 * Ad content is chosen randomly from the URLs listed in inventory.json.
 */
(function (window, document, vjs, undefined) {
    "use strict";

    var registerPlugin = vjs.registerPlugin || vjs.plugin;
    var domain = window.location.host || 'videojs';
    var storageKey = domain + ':';

    /*
     * Register the ad plugin.
     * To initialize for a player, call player.exampleAds().
     *
     * @param {mixed} options Hash of obtions for the exampleAds plugin.
     */
    registerPlugin('adService', function (options) {

        var

            player = this,

            // example plugin state, may have any of these properties:
            //  - inventory - hypothetical ad inventory, list of URLs to ads
            //  - lastTime - the last time observed during content playback
            //  - adPlaying - whether a linear ad is currently playing
            //  - prerollPlayed - whether we've played a preroll
            //  - midrollPlayed - whether we've played a midroll
            //  - postrollPlayed - whether we've played a postroll
            state = {
                "totalTimePlayed": 0,
                "auxTime": 0,
                "adPlaying": false
            },

            // just like any other video.js plugin, ad plugins can
            // accept initialization options
            adToPlay = 0,
            adList = (options && options.adList) || false,
            adServerUrl = (options && options.adServerUrl) || "inventory.json",
            // midrollPoint = (options && options.midrollPoint) || 15,
            playPreroll = options && options.playPreroll !== undefined ? options.playPreroll : true,
            playMidroll = options && options.playMidroll !== undefined ? options.playMidroll : true,
            playPostroll = options && options.playPostroll !== undefined ? options.playPostroll : true,
            intervalForceAds = options && options.intervalForceAds ? options.intervalForceAds : false,

            // asynchronous method for requesting ad inventory
            // requestAds = function () {

            //     // reset plugin state
            //     state = {};

            //     // fetch ad inventory
            //     // the 'src' parameter is ignored by the example inventory.json flat file,
            //     // but this shows how you might send player information along to the ad server.
            //     var xhr = new XMLHttpRequest();
            //     xhr.open("GET", adServerUrl + "?src=" + encodeURIComponent(player.currentSrc()));
            //     xhr.onreadystatechange = function () {
            //         if (xhr.readyState === 4) {
            //             try {
            //                 state.inventory = JSON.parse(xhr.responseText);
            //                 player.trigger('adsready');
            //             } catch (err) {
            //                 throw new Error('Couldn\'t parse inventory response as JSON');
            //             }
            //         }
            //     };
            //     xhr.send(null);

            // },

            forcedAds = function () {
                clearTimeout(intervalForceAds);
                // fetch ad inventory
                // the 'src' parameter is ignored by the example inventory.json flat file,
                // but this shows how you might send player information along to the ad server.
                // console.log('forcedAds');
                var xhr = new XMLHttpRequest();
                xhr.open("GET", adServerUrl + "?type=forced");
                xhr.onreadystatechange = function () {
                    if (xhr.readyState === 4) {
                        try {
                            state.inventory['forced'] = JSON.parse(xhr.responseText);
                            player.trigger('adsforced');
                        } catch (err) {
                            throw new Error('Couldn\'t parse inventory response as JSON');
                        }
                    }
                };
                xhr.send(null);

            },

            adInRange = function (horaInicio, horaFin) {
                if (new Date().getTime() < new Date(horaInicio).getTime() || new Date().getTime() > new Date(horaFin).getTime()) return false;
                return true;
            },

            // play an ad, given an opportunity
            playAd = function (type = false, index = false, hasMore = false) {
                if (!type) {
                    videojs.log('Type not defined.');
                    return;
                }

                // short-circuit if we don't have any ad inventory to play
                if (!state.inventory || state.inventory.length === 0) {
                    videojs.log('No inventory to play.');
                    return;
                }

                console.log('index', index);
                if (index) adToPlay = index;

                // tell videojs to load the ad
                // var media = state.inventory[Math.floor(Math.random() * state.inventory.length)];
                var media = state.inventory[type][adToPlay];
                // console.log('media');
                // console.log(media);
                if (!media) return;
                if (adInRange(media.hora_inicio, media.hora_fin) === false) return;

                var keyAd = storageKey + 'forced:' + media.uuid;
                if (type === 'forced') {
                    var played = localStorage.getItem(keyAd);
                    if (played) return;
                }

                // tell ads plugin we're ready to play our ad
                console.log('adToPlay', adToPlay);
                console.log('type', type)
                console.log('media', media.id)
                if (hasMore === false && (adToPlay === 0 || type === 'midroll' || type === 'forced')) {
                    console.log('asdkjakjsdfa');
                    player.ads.startLinearAdMode();
                }
                state.adPlaying = true;

                player.src(media);
                localStorage.setItem(keyAd, true);

                if (hasMore === false && (adToPlay === 0 || type === 'midroll' || type === 'forced')) {
                    player.trigger('ads-ad-started');
                }

                // when it's finished
                player.one('adended', function () {

                    adToPlay++;
                    console.log('adended', type)
                    console.log('adToPlay', adToPlay)
                    if (type === 'preroll') {
                        var nextAd = state.inventory['preroll'][adToPlay];
                        if (nextAd) {
                            if (adInRange(nextAd.hora_inicio, nextAd.hora_fin) === true) return playAd('preroll', false, true);
                        }
                    }

                    if (type === 'forced') {
                        var nextAd = state.inventory['forced'][adToPlay];
                        if (nextAd) {
                            if (adInRange(nextAd.hora_inicio, nextAd.hora_fin) === true) return playAd('forced', false, true);
                        }
                    }

                    adToPlay = 0;
                    player.trigger('ads-ad-ended');

                    // play your linear ad content, then when it's finished ...
                    player.ads.endLinearAdMode();
                    state.adPlaying = false;

                    if (type === 'preroll') player.trigger('prerollended');
                });

            };

        console.log(player);
        // initialize the ads plugin, passing in any relevant options
        player.ads(options);

        // request ads right away
        // requestAds();
        // if (intervalForceAds) {
        //     intervalForceAds = setTimeout(forcedAds, 10000);
        // }

        if (adList) {
            state.inventory = adList;
            player.trigger('adsready');
        }

        player.on('adsready', function () {
            if (!playPreroll || state.inventory['preroll'].length === 0) {
                player.trigger('nopreroll');
            }
        });

        player.on('prerollended', function () {
            if (intervalForceAds) {
                intervalForceAds = setTimeout(forcedAds, 5000);
            }
        });

        player.on('adsforced', function () {
            // console.log('<- adsforced ->');
            // console.log('*********')
            // console.log(state.inventory);
            console.log('*********')
            console.log(state.adPlaying);
            if (state.adPlaying === false) {

                if (state.inventory.forced && state.inventory.forced.length !== 0) {
                    playAd('forced');
                }
            }

            intervalForceAds = setTimeout(forcedAds, 5000);
        });

        // request ad inventory whenever the player gets content to play
        player.on('contentchanged', function () {
            console.log('<- contentchanged ->');
            // requestAds();
            // intervalForceAds = setTimeout(forcedAds, 5000);
        });

        player.on('readyforpostroll', function () {
            if (!state.postrollPlayed && playPostroll) {
                state.postrollPlayed = true;
                playAd();
            } else {
                player.trigger('nopostroll');
            }
        });

        // play an ad the first time there's a preroll opportunity
        player.on('readyforpreroll', function () {
            console.log('<-readyforpreroll->')
            if (state.inventory.preroll.length === 0) return player.trigger('prerollended');

            if (!state.prerollPlayed && playPreroll) {
                state.prerollPlayed = true;
                return playAd('preroll');
            }
        });

        // watch for time to pass 15 seconds, then play an ad
        // if we haven't played a midroll already
        var lastSeconds = 0;
        // var firstMidRoll = true;
        player.on('timeupdate', function (event) {

            // if (state.midrollPlayed) {
            //     return;
            // }

            // var currentTime = player.currentTime(), opportunity;

            // if ('lastTime' in state) {
            //     opportunity = currentTime > midrollPoint && state.lastTime < midrollPoint;
            // }

            // state.lastTime = currentTime;
            // if (opportunity && playMidroll) {
            //     state.midrollPlayed = true;
            //     playAd();
            // }

            var currentTime = player.currentTime();
            var seconds = parseInt(currentTime, 10);

            if ('totalTimePlayed' in state) {
                if (playMidroll && state.inventory && state.inventory.midroll.length !== 0) {
                    for (var i = 0; i < state.inventory.midroll.length; i++) {
                        var midAd = state.inventory.midroll[i];
                        // state.inventory.midroll.forEach(function (midAd, i) {
                        if (state.adPlaying === false) {
                            if (adInRange(midAd.hora_inicio, midAd.hora_fin) === true) {
                                adToPlay = i;
                                if (midAd.mostrar === 'one-time' && state.totalTimePlayed == midAd.delay) {
                                    // console.log('***')
                                    // console.log('state.adPlaying', state.adPlaying)
                                    // console.log('i adToPlay', adToPlay)
                                    // console.log('midAd', midAd)
                                    // console.log('state.totalTimePlayed ', state.totalTimePlayed)
                                    // console.log('midAd.delay')
                                    // state.adPlaying = true;
                                    // if (firstMidRoll === true) {
                                    //     firstMidRoll = false;
                                    playAd('midroll', i);
                                    // } else {
                                    //     playAd('midroll', i, true);
                                    // }
                                    console.log('----')
                                    state.inventory.midroll.splice(i, 1);
                                    // i--;
                                    return;
                                }
                                // else if (midAd.mostrar === 'repeat' && state.auxTime == midAd.delay) {
                                //     playAd('midroll');
                                // }
                                state.auxTime++;
                                // state.totalTimePlayed ++;
                                // return;
                            }
                        }
                        // });
                    }
                }

                state.auxTime = seconds;
                if (lastSeconds != seconds) state.totalTimePlayed += 1;
                lastSeconds = seconds;
            }

        });

    });

})(window, document, videojs);
