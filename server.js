const path = require('path');
const https = require("https");
const fs = require("fs");
const express = require("express");
const morgan = require("morgan");
const compression = require("compression");
const bodyParser = require("body-parser");
const helmet = require('helmet');
const exphbs = require('express-handlebars');
const session = require('express-session');

const port = 80;
const portHttps = 443;
const hbsHelpers = require('./utils/handlebarsHelpers');
const SQL = require('./utils/mysql');

const app = express();
const SessionStore = require('connect-redis')(session);

app.engine('.hbs', exphbs({
    extname: '.hbs',
    defaultLayout: 'main',
    layoutsDir: path.join(__dirname, 'views/layouts/'),
    partialsDir: path.join(__dirname, 'views/partials/'),
    helpers: hbsHelpers
}));

app.set('trust proxy');
app.set('view engine', '.hbs');
app.set('views', path.join(__dirname, 'views'));
app.use(express.static(path.join(__dirname, 'public')));
// app.use(morgan("common"));
app.use(morgan("combined"));
app.use(helmet());
app.use(compression());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(session({
    store: new SessionStore({
        host: "127.0.0.1",
        port: 6379
    }),
    secret: '127.0.0.1',
    resave: false,
    saveUninitialized: false
}));

app.get('/', async (req, res) => {
    if (req.session && req.session.user && req.session.user.authenticated) {
        return res.redirect(`/player`);
    }

    return res.redirect('/auth/login');
});

app.use('/auth', require('./routes/auth'));

app.use((req, res, next) => {
    if (req.session && req.session.user && req.session.user.authenticated) {
        return next();
    }
    return res.redirect('/auth/login');
});

// SQL
app.get('/delete/ad/:id', async (req, res) => {
    try {
        const remove = await SQL(`DELETE FROM ads WHERE id = '${req.params.id}'`);
        console.log(remove);
        return res.json({ success: true });
    } catch (e) {
        console.log('Error delete');
        console.log(e);
        return res.json({ success: false, error: e });
    }
});

app.get('/ads', async (req, res) => {
    const type = req.query.type || false;
    if (type === 'forced') {
        return res.json(await SQL(`SELECT * FROM ads WHERE hora_fin > DATE_SUB('${new Date().toISOString()}', INTERVAL 2 HOUR) AND tipo_anuncio = 'forced' AND activo = 1`));
    }

    const prerolls = await SQL(`SELECT * FROM ads WHERE hora_fin > DATE_SUB('${new Date().toISOString()}', INTERVAL 2 HOUR) AND tipo_anuncio = 'preroll' AND activo = 1`);
    const midrolls = await SQL(`SELECT * FROM ads WHERE hora_fin > DATE_SUB('${new Date().toISOString()}', INTERVAL 2 HOUR) AND tipo_anuncio = 'midroll' AND activo = 1`);
    const postrolls = await SQL(`SELECT * FROM ads WHERE hora_fin > DATE_SUB('${new Date().toISOString()}', INTERVAL 2 HOUR) AND tipo_anuncio = 'postroll' AND activo = 1`);
    // const forced = await SQL(`SELECT * FROM ads WHERE tipo_anuncio = 'forced'`);

    const toReturn = {
        "preroll": prerolls,
        "midroll": midrolls,
        "postroll": postrolls,
        // "forced": forced
    };

    return res.json(toReturn);
});

app.post('/update/ad', async (req, res) => {
    console.log(req.body);
    try {
        const id = req.body.id || false;

        if (!id) {
            return res.json({ success: false, error: 'No hay video' });
        }

        const update = await SQL(`UPDATE ads SET ? WHERE id = ?`, [req.body, id]);
        console.log(update);
        res.json({ success: true });
    } catch (e) {
        console.log('Error');
        console.log(e);
        res.json({ success: false, error: e });
    }
});

const uuidv4 = require('uuid/v4');
app.post('/ads', async (req, res) => {
    try {
        console.log(req.body);
        const {
            tipo_anuncio,
            mostrar,
            hora_inicio,
            hora_fin,
            delay,
            src,
            activo,
            type
        } = { ...req.body };

        if (!tipo_anuncio || tipo_anuncio == '') { return res.json({ success: false, error: 'tipo_anuncio' }); }
        if (!mostrar || mostrar == '') { return res.json({ success: false, error: 'mostrar' }); }
        if (!hora_inicio || hora_inicio == '') { return res.json({ success: false, error: 'hora_inicio' }); }
        if (!hora_fin || hora_fin == '') { return res.json({ success: false, error: 'hora_fin' }); }
        if (!delay || delay == '') { return res.json({ success: false, error: 'delay' }); }
        if (!src || src == '') { return res.json({ success: false, error: 'src' }); }
        if (!type || type == '') { return res.json({ success: false, error: 'type' }); }

        req.body['uuid'] = uuidv4();

        await SQL(`INSERT INTO ads SET ?`, req.body);
        res.json({ success: true });
    } catch (e) {
        console.log('Error');
        console.log(e);
        res.json({ success: false, error: e });
    }
});

app.get('/gestor', async (req, res) => {
    const json = req.query.json || false;
    if (json) { return res.json(await SQL(`SELECT * FROM ads WHERE hora_fin > DATE_SUB('${new Date().toISOString()}', INTERVAL 2 HOUR) ORDER BY FIELD(tipo_anuncio, 'forced') DESC, hora_inicio, hora_fin LIMIT 50`)); }

    const ads = await SQL(`SELECT * FROM ads WHERE hora_fin > DATE_SUB('${new Date().toISOString()}', INTERVAL 2 HOUR) ORDER BY FIELD(tipo_anuncio, 'forced') DESC, hora_inicio, hora_fin LIMIT 50`);
    return res.render('gestor', { ads });
});


app.get('/player', (req, res) => {
    const hideNav = req.query.hideNav || false;
    const optionsPlayer = {
        url: "https://api.goltelevision.com/api/v1/media/hls/service/live",
    }
    return res.render('player', { hideNav, optionsPlayer });
});

app.listen(port);

const options = {
    ca: fs.readFileSync(process.cwd() + "/certs/gd_bundle-g2-g1.crt"),
    key: fs.readFileSync(process.cwd() + "/certs/goltelevision.com.key"),
    cert: fs.readFileSync(process.cwd() + "/certs/c7bb4a6f4274dd91.crt"),
    passphrase: 'mediapro'
};
https.createServer(options, app).listen(portHttps);