const express = require('express');
const { form, login, logout } = require('../controllers/auth');

const router = express.Router();

router.route('/login')
    .get(form)
    .post(login);
router.get('/logout', logout);

module.exports = router;